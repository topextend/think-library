<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Date         : 2021-07-29 17:30:09
// |@----------------------------------------------------------------------
// |@LastEditTime : 2022-10-13 21:09:46
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : MenuService.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2021 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);

namespace think\admin\service;

use think\admin\extend\DataExtend;
use think\admin\model\SystemMenu;
use think\admin\Service;

/**
 * 系统菜单管理服务
 * Class MenuService
 * @package app\admin\service
 */
class MenuService extends Service
{
    /**
     * 获取可选菜单节点
     * @param boolean $force 强制刷新
     * @return array
     * @throws \ReflectionException
     */
    public static function getList(bool $force = false): array
    {
        if (empty($force)) {
            static $nodes = [];
            if (count($nodes) > 0) return $nodes;
        } else {
            $nodes = [];
        }
        foreach (NodeService::getMethods($force) as $node => $method) {
            if ($method['ismenu']) $nodes[] = ['node' => $node, 'title' => $method['title']];
        }
        return $nodes;
    }

    /**
     * 获取系统菜单树数据
     * @return array
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getTree(): array
    {
        $query = SystemMenu::mk()->where(['status' => 1]);
        $menus = $query->order('sort desc,id asc')->select()->toArray();
        if (function_exists('admin_menu_filter')) admin_menu_filter($menus);
        return static::build(DataExtend::arr2tree($menus));
    }

    /**
     * 后台主菜单权限过滤
     * @param array $menus 当前菜单列表
     * @return array
     * @throws \ReflectionException
     */
    private static function build(array $menus): array
    {
        foreach ($menus as $key => &$menu) {
            if (!empty($menu['sub'])) {
                $menu['sub'] = static::build($menu['sub']);
            }
            if (!empty($menu['sub'])) {
                $menu['url'] = '#';
            } elseif ($menu['url'] === '#') {
                unset($menus[$key]);
            } elseif (preg_match('/^(https?:)?(\/\/|\\\\)/i', $menu['url'])) {
                if (!!$menu['node'] && !AdminService::check($menu['node'])) {
                    unset($menus[$key]);
                } elseif ($menu['params']) {
                    $menu['url'] .= (strpos($menu['url'], '?') === false ? '?' : '&') . $menu['params'];
                }
            } elseif (!!$menu['node'] && !AdminService::check($menu['node'])) {
                unset($menus[$key]);
            } else {
                $node = join('/', array_slice(explode('/', $menu['url']), 0, 3));
                $menu['url'] = url($menu['url'])->build() . ($menu['params'] ? '?' . $menu['params'] : '');
                if (!AdminService::check($node)) unset($menus[$key]);
            }
        }
        return $menus;
    }
}